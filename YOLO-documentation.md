
# YOLO documentation

I'm compiling here all the material to understand better YOLO.

Built by: andre@brainyt.com

## YOLO masterclass @TensorFlow with Siraj Raval

[![](http://img.youtube.com/vi/4eIBisqx9_g/0.jpg)](http://www.youtube.com/watch?v=4eIBisqx9_g "YOLO Object Detection - TensorFlow tutorial")

## Further links and readings about YOLO

- [YOLO — ‘You only look once’ for Object Detection explained](https://medium.com/diaryofawannapreneur/yolo-you-only-look-once-for-object-detection-explained-6f80ea7aaa1e)
- [Real time vehicle detection using YOLO](https://medium.com/@xslittlegrass/almost-real-time-vehicle-detection-using-yolo-da0f016b43de)
- [Easy YOLO](https://github.com/KleinYuan/easy-yolo)
- [YOLO: Real-Time Object Detection](https://github.com/pjreddie/darknet/wiki/YOLO:-Real-Time-Object-Detection)
- [Real-time object detection with YOLO](http://machinethink.net/blog/object-detection-with-yolo/)
- [How to train YOLOv2 to detect custom objects](https://timebutt.github.io/static/how-to-train-yolov2-to-detect-custom-objects/)
- [Pjreddie YOLO website with Darknet](https://pjreddie.com/darknet/yolo/)
- [Siraj GitHub DarkFlow Tutorial](https://github.com/llSourcell/YOLO_Object_Detection)
- [YOLO original publications - papers, talk, slides and arXiv](https://pjreddie.com/publications/)
- [AlexeyAB GitHub - Trainning tips](https://github.com/AlexeyAB/darknet)
- [Explanation for YOLO parameters in yolo.cfg file](https://groups.google.com/forum/#!topic/darknet/Fe0BhzWPTeU)
- [A Beginner's Guide To Understanding Convolutional Neural Networks](https://adeshpande3.github.io/A-Beginner%27s-Guide-To-Understanding-Convolutional-Neural-Networks/) (Artigo 1 de 3)
- [A Beginner's Guide To Understanding Convolutional Neural Networks Part 2](https://adeshpande3.github.io/adeshpande3.github.io/A-Beginner's-Guide-To-Understanding-Convolutional-Neural-Networks-Part-2/) (Artigo 2 de 3)
- [The 9 Deep Learning Papers You Need To Know About (Understanding CNNs Part 3)](https://adeshpande3.github.io/adeshpande3.github.io/The-9-Deep-Learning-Papers-You-Need-To-Know-About.html) (Artigo 3 de 3)
- [Understanding darknet's yolo.cfg config files - Stackoverflow](https://stackoverflow.com/questions/50390836/understanding-darknets-yolo-cfg-config-files)
- [How to train YOLOv2 to detect custom objects](https://medium.com/@manivannan_data/how-to-train-yolov2-to-detect-custom-objects-9010df784f36)
- [Understanding YOLOv2 training output](https://timebutt.github.io/static/understanding-yolov2-training-output/)
- [Darknet Google Groups](https://groups.google.com/forum/?nomobile=true#!forum/darknet)
- 
